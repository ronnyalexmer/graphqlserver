const MULTAS_URL = {
    ALL_MULTAS: "http://localhost:8080/MultasWEB-1.0-SNAPSHOT/multas/showAll",
    MULTAS_BY_PERIOD: "http://localhost:8080/MultasWEB-1.0-SNAPSHOT/multas/date"
};

module.exports = MULTAS_URL;
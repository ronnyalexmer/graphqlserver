const express = require('express');
const bodyParser = require('body-parser');
const {ApolloServer} = require('apollo-server');

const PORT = 4200;

const typeDefs = require("./api/schemas/schema");
const resolvers = require("./api/resolvers/resolvers");

const SERVER = new ApolloServer({
    typeDefs,
    resolvers
});

SERVER.listen({port: PORT}).then(({url}) => {
    console.log(`URL: ${url}`);
});

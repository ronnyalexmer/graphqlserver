const multasController = require("../controllers/multasController");
let multas = [];
multasController.allMultas(function (data) {
    data.forEach(multa => {
        multa.persona.gender = multa.persona.gender.toUpperCase();
    });
    multas = data;
});

const resolvers = {
    Query: {
        allPeople: () => {
            let persons = [];
            multas.forEach(multa => {
                persons.push(multa.persona);
            });
            return persons;
        },
        person: (root, args) => {
            return multas.filter(multa => {
                return (multa.persona.email === args.email);
            })[0].persona;
        },
        personByGender: (root, args) => {
            let persons = [];
            multas.filter(multa => {
                if (multa.persona.gender === args.gender) {
                    persons.push(multa.persona)
                }
            });
            return persons;
        },
        searchByGenderName: (root, args) => {
            let persons = [];
            multas.filter(multa => {
                return (multa.persona.gender === args.gender);
            }).filter(multa => {
                if (multa.persona.name.toLowerCase().indexOf(args.name.toLowerCase()) > -1) {
                    persons.push(multa.persona);
                }
            });
            return persons;
        }
    }
};

module.exports = resolvers;
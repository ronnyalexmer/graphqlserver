import {GraphQLList, GraphQLNonNull} from "graphql";

const {
    GraphQLObjectType
} = require("graphql");
import personType from "./person";
import genderEnum from "./person";

const AllPerson = new GraphQLObjectType({
    name: "PersonByGender",
    fields: {
        persons: {
            name: "persons",
            type: new GraphQLList(personType)
        }
    }
});

const PersonByGender = new GraphQLObjectType({
    name: "PersonByGender",
    fields: {
        persons: {
            name: "persons",
            type: new GraphQLList(personType),
            arguments: {
                gender: {
                    type: genderEnum
                }
            }
        }
    }
});

const schema = new GraphQLSchema({
    query: AllPerson
});
module.exports = schema;


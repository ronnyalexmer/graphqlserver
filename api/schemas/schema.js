//const {makeExecutableSchema} = require("graphql-tools");
const {gql} = require("apollo-server-express");
const typeDefs = gql`
    enum Gender{
        MALE
        FEMALE
        BOT
    }

    type Person {
        email: String
        name: String
        gender: Gender!
        age: Int
    }
    
    type Vehiculo {
        marca: String
        modelo: String
        cc: String
    }
    
    type Multas {
        fecha: String
        lugar: String
        importe: Float
        persona: Person
        vehiculo: Vehiculo        
    }
    
    type Query {
        allPeople: [Person]
        person(email: String!): Person
        personByGender(gender: Gender) : [Person]
        searchByGenderName(gender: Gender, name: String!) : [Person]
    }
`;

module.exports = typeDefs;
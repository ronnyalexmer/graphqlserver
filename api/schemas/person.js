const {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLInt,
    GraphQLString,
    GraphQLEnumType
} = require("graphql");

const genderEnum = new GraphQLEnumType({
    name: "gender",
    values: {
        MALE: {value: "MALE"},
        FEMALE: {value: "FEMALE"}
    }
});

const personType = new GraphQLObjectType({
    name: "person",
    fields: {
        name: {
            type: new GraphQLNonNull(GraphQLString)
        },
        gender: {
            type: genderEnum
        },
        age: {
            type: GraphQLInt
        }
    }
});
export default personType;
export default genderEnum;

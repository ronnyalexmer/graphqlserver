const MULTAS_URL = require("../../constants/multas");

const fetch = require('node-fetch');

const multasController = (function () {
    const allMultas = (callback) => {
        return fetch(MULTAS_URL.ALL_MULTAS)
            .then(res => res.json())
            .then(json => callback(json));
    };

    return {
        allMultas: allMultas
    }
})();


module.exports = multasController;